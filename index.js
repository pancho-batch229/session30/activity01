/*

db.fruits.aggregate([
    {$match:{$and:[{supplier: "Yellow Farms"},{price:{$lt:50}}]}},
    {$count:"yellowFarmsItemsBelow50"}
])

db.fruits.aggregate([
    {$match:{price:{$lt:30}}},
    {$count:"itemsBelow30"}
]) 


db.fruits.aggregate([
    {$match:{supplier:"Yellow Farms"}},
    {$group:{_id:"YelFarmAveragePrice",averagePrice:{$avg:"$price"}}}
])

db.fruits.aggregate([
    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"RedFarmsMaxPrice",maxPrice:{$max:"$price"}}}
])

db.fruits.aggregate([
    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"RedFarmsLowestPrice",minPrice:{$min:"$price"}}}
])

*/